const form = document.getElementById('form');
const qr = document.getElementById('qrcode');

const onFormSubmit = (e) => {
  e.preventDefault();
  clearUi();

  const url = document.getElementById('url').value;
  const size = document.getElementById('size').value;

  if (url === '') {
    alert('Please insert URL');
  } else {
    showSpinner();

    setTimeout(() => {
      hideSpinner();
      generateQR(url, size);

      setTimeout(() => {
        const saveUrl = qr.querySelector('img').src;
        createSaveBtn(saveUrl);
      }, 50);
    }, 1000);
  }
};

const generateQR = (url, size) => {
  const qrcode = new QRCode('qrcode', {
    text: url,
    width: size,
    height: size,
  });
};

const showSpinner = () => {
  document.getElementById('spinner').style.display = 'block';
};

const hideSpinner = () => {
  document.getElementById('spinner').style.display = 'none';
};

const clearUi = () => {
  qr.innerHTML = '';
  const saveLink = document.getElementById('save-link');
  if (saveLink) saveLink.remove();
};

const createSaveBtn = (saveUrl) => {
  const link = document.createElement('a');
  link.id = 'save-link';
  link.classList =
    'bg-amber-500 hover:bg-amber-600 text-amber-50 font-bold py-2 rounded w-1/3 m-auto my-5';
  link.href = saveUrl;
  link.download = 'qrcode';
  link.innerHTML = 'Save Image';
  document.getElementById('generated').appendChild(link);
};

hideSpinner();

form.addEventListener('submit', onFormSubmit);
